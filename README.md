#Um simples analizador léxico para o trabalho de Compiladores

##Requisitos
*	flex
*	gcc

##Ambiente em que já foi testado
*	Sistema Operacional Linux, distribuição elementary OS 0.4 Loki, baseada no Ubuntu 16.04
*	gcc versão 5.4.0
*	flex versão 2.6.0

###Como compilar
Num terminal de um sistema unix navegue até o diretório raiz e simplesmente digite 

`make`

Serão gerados os arquivos `src/lex.yy.c` e o `analisador.exe`. O primeiro é o arquivo gerado na compilação através do programa flex e o segundo da compilação dele com o gcc.

###Como executar
Num terminal de um sistema unix navegue até o diretório raiz e simplesmente digite

`make run <path/to/arquivo_com_a_entrada.lagl`

Obs.: O programa já deve ter sido compilado

###Como limpar

Para excluir os arquivos gerados na compilação, num terminal de um sistema unix navegue até o diretório raiz e simplesmente digite

`make clean`

Os arquivos `src/lex.yy.x` e o `analisador.exe` serão excluídos.
