%{
	#include <stdio.h>
	#include <string.h>

	#define RED     "\x1b[31m"
	#define GREEN   "\x1b[32m"
	#define YELLOW  "\x1b[33m"
	#define BLUE    "\x1b[34m"
	#define MAGENTA "\x1b[35m"
	#define CYAN    "\x1b[36m"
	#define COLOR_RESET   "\x1b[0m"
%}

%%

program {
	printf(YELLOW "%s - PROGRAM\n" COLOR_RESET, yytext);
}

begin {
	printf(YELLOW "%s - BEGIN\n" COLOR_RESET, yytext);
}

end {
	printf(YELLOW "%s - END\n" COLOR_RESET, yytext);
}

var {
	printf(BLUE "%s - VAR\n" COLOR_RESET, yytext);
}

integer {
	printf(CYAN "%s - INT\n" COLOR_RESET, yytext);
}

real {
	printf(CYAN "%s - REAL\n" COLOR_RESET, yytext);
}

procedure {
	printf(GREEN "%s - PROCDRE\n" COLOR_RESET, yytext);
}

else {
	printf(GREEN "%s - ELSE\n" COLOR_RESET, yytext);
}

readln {
	printf("%s - READ\n", yytext);
}

writeln {
	printf("%s - WRITE\n", yytext);
}

if {
	printf(GREEN "%s - IF\n" COLOR_RESET, yytext);
}

while {
	printf(GREEN "%s - CMD_WHILE\n" COLOR_RESET, yytext);
}

do {
	printf(GREEN "%s - DO\n" COLOR_RESET, yytext);
}


\. {
	printf("%s - PERIOD\n", yytext);
}

, {
	printf("%s - COMMA\n", yytext);
}

; {
	printf("%s - SEMICO\n", yytext);
}

: {
	printf("%s - COLON\n", yytext);
}

\( {
	printf("%s - OPAR\n", yytext);
}

\) {
	printf("%s - CPAR\n", yytext);
}

[0-9]([0-9])* {
	printf(BLUE "%s - NUM_INT\n" COLOR_RESET, yytext);
}

([0-9]+)[\.]([0-9]+) {
	printf(BLUE "%s - NUM_REAL\n" COLOR_RESET, yytext);
}


[a-zA-Z]([a-zA-Z]|[0-9]|[-]|[_])* {
	printf(BLUE "%s - ID\n" COLOR_RESET, yytext);
}

[<|>|=|<>|>=|<=] {
	printf("%s - REL\n", yytext);
}

[:][=] {
	printf(GREEN "%s - ATTR\n" COLOR_RESET, yytext);
}

[:][=]([\t\n]+)[+|-] {
	char buff = yytext[strlen(yytext)];
	printf(BLUE "%c - SIGN\n" COLOR_RESET, buff);
}

[+|-] { 
	printf(GREEN "%s - OP_ADD\n" COLOR_RESET, yytext);
}

[*|/] {
	printf(GREEN "%s - OP_MUL\n" COLOR_RESET, yytext);
}

\{(.)*[\}]* {
	/*Ignora comentários*/;
	int strl;
	int i, j;

	strl = strlen(yytext);
	j = 0;
	for(i = 0; i < strl; i++) {
		if(yytext[i] == '}') {
			j = 1;
		}
	}

	if(!j) {
		printf(RED "%s - ERRO: Comentario nao finalizado na mesma linha\n" COLOR_RESET, yytext);
	}
}

[ \t]+ /*Ignora espaços em branco*/;

\n /*Ignora as quebras de linha*/;
	
[.]+ {
	/*Trata qualquer caracter que não esteja na linguagem como erro*/;
	printf(RED "%s - ERRO: Caracter nao reconhecido\n" COLOR_RESET, yytext);
}

([0-9]+)([a-zA-Z])*([0-9])* {
	/*Trata o erro de uma letra dentro de um número*/;
	printf(RED "%s - ERRO: Numero mal formado\n" COLOR_RESET, yytext);
}

%%

int main(void) {
	yylex();
	printf("\n");
}