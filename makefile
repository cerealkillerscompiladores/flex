#Compiladores utilizados
CC = gcc
FCC = flex

#Flags necessárias para compilação
FLAGS = -lfl

#Diretórios do programa
SDIR = ./src

#Arquivo executavel
EXEC = analisador.exe

#Arquivo fonte gerado
LEX = $(SDIR)/lex.yy.c

all: $(EXEC)

$(LEX): $(SDIR)/lex.l
	$(FCC) -o $@ $(SDIR)/lex.l

$(EXEC): $(LEX)
	$(CC) $(LEX) -o $@ $(FLAGS) -g3

run:
	./$(EXEC)

.PHONY: clean
clean:
	rm $(LEX)
	rm $(EXEC)